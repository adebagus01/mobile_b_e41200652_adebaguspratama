import 'package:flutter/material.dart';
import 'package:minggu5/routes.dart';

void main() {
  runApp(MaterialApp(
    onGenerateRoute: RouteGenerator.generateRoute,
  ));
}
